package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"syscall"

	"git.fractalqb.de/fractalqb/yacfg"
	_ "git.fractalqb.de/fractalqb/yacfg/yaml"
	"git.fractalqb.de/fractalqb/yacfg/yasec"
	"golang.org/x/term"
)

const yasecUnixSocket = "master-password.yasec"

var config = struct {
	Log             string
	Password        yasec.Secret // Uses yasec.DefaultConfig
	YasecNoTerminal string       // values: fd, unix
}{
	Log: "info",
}

func main() {
	fmt.Println("Run with '-config config.yasec.json' to load the demo configuration.")
	fmt.Println("I tell you te master password for demo reasons: secret")

	yacfg.FromEnvThenFiles{
		EnvPrefix:     "CFGS_",
		FilesFlagName: "config",
		Flags:         flag.CommandLine,
		Log:           func(m string) { fmt.Println(m) },
	}.MustConfigure(&config)

	flag.StringVar(&config.Log, "log", config.Log,
		"Set log level",
	)
	flag.StringVar(&config.YasecNoTerminal, "yasec", config.YasecNoTerminal,
		`Select source for master password when stdin is not a terminal:
fd:	Read password from file descriptor 0, i.e. stdin
unix:	Read password from a unix domain socket`,
	)
	flag.Parse()

	// Ignoring password input error for brevity
	if term.IsTerminal(int(os.Stdin.Fd())) {
		yasec.DefaultConfig.SetFromPrompt("yasec master password:", nil)
	} else {
		switch config.YasecNoTerminal {
		case "fd":
			log.Println("reading yasec master password from stdin")
			yasec.DefaultConfig.SetPasswordFrom(yasec.FDPassword(syscall.Stdin), nil)
		case "unix":
			log.Println("write your yasec master password to", yasecUnixSocket)
			yasec.DefaultConfig.SetFromUnixSocket(yasecUnixSocket, nil)
		default:
			log.Printf("invalid yasec password source: '%s'", config.YasecNoTerminal)
			log.Println("setting the correct secret")
			yasec.DefaultConfig.SetPasswordString("secret", nil)
		}
	}

	enc := json.NewEncoder(os.Stdout)
	enc.SetIndent("", "  ")
	enc.Encode(&config)

	pw, err := config.Password.Open()
	if err != nil {
		log.Fatal(err)
	}
	defer pw.Destroy()
	fmt.Println("Password:", pw.String())
}
