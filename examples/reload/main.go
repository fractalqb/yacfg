package main

import (
	"log"
	"os"
	"os/signal"

	"git.fractalqb.de/fractalqb/yacfg/yachg"
	"github.com/fractalqb/change"
)

var cfg = struct {
	Log yachg.Obs[string]
}{
	Log: yachg.NewObs("info", nil, 1, change.UpdateFunc(func(_ any, e change.Event) {
		ce := e.(change.Changed[string])
		log.Printf("Log '%s' changed to: '%s'\n", ce.OldValue(), ce.NewValue())
	})),
}

func main() {
	go yachg.Reload(&cfg, yachg.StdLog(nil), "config.json")
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt)
	<-sigs
}
