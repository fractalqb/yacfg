package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"time"

	"git.fractalqb.de/fractalqb/yacfg"
	_ "git.fractalqb.de/fractalqb/yacfg/yaml"
)

type SubPkg struct {
	SrvAddr string
	Timeout time.Duration
}

var config = struct {
	Log    string
	SubPkg SubPkg
}{
	Log: "info",
	SubPkg: SubPkg{
		SrvAddr: "localhost:8080",
		Timeout: 5 * time.Second,
	},
}

func main() {
	yacfg.FromEnvThenFiles{
		EnvPrefix:     "CFGS_",
		FilesFlagName: "config",
		Flags:         flag.CommandLine,
		Log:           func(m string) { fmt.Println(m) },
	}.MustConfigure(&config)

	flag.StringVar(&config.SubPkg.SrvAddr, "srv-addr", config.SubPkg.SrvAddr,
		"Set server address")
	flag.DurationVar(&config.SubPkg.Timeout, "srv-timeout", config.SubPkg.Timeout,
		"Set server timeout duration")
	flag.StringVar(&config.Log, "log", config.Log,
		"Set log level")
	flag.Parse()

	enc := json.NewEncoder(os.Stdout)
	enc.SetIndent("", "  ")
	enc.Encode(&config)
}
