// Package yachg provides observable config values and tools to watch config
// files for changes while a program is running.
package yachg

import (
	"log"

	"git.fractalqb.de/fractalqb/yacfg"
	"github.com/rjeczalik/notify"
)

type ReloadLog interface {
	WatchError(path string, err error)
	Reload(path string)
	ReloadError(path string, err error)
}

func StdLog(l *log.Logger) ReloadLog {
	if l == nil {
		l = log.Default()
	}
	return stdLogger{l}
}

func Reload(cfg any, log ReloadLog, files ...string) error {
	c := make(chan notify.EventInfo, 1)
	defer notify.Stop(c)
	for _, f := range files {
		err := notify.Watch(f, c, notify.Write)
		if err != nil {
			if log != nil {
				log.WatchError(f, err)
			}
			return err
		}
	}
	for e := range c {
		_, err := yacfg.Files(cfg, log.Reload, e.Path())
		if err != nil && log != nil {
			log.ReloadError(e.Path(), err)
		}
	}
	return nil
}

type stdLogger struct {
	l *log.Logger
}

func (log stdLogger) WatchError(path string, err error) {
	log.l.Printf("yachg watch '%s' error: %s", path, err)
}

func (log stdLogger) Reload(path string) {
	log.l.Printf("yachg reload '%s'", path)
}

func (log stdLogger) ReloadError(path string, err error) {
	log.l.Printf("yachg reload error: %s", err)
}
