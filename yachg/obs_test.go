package yachg

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/fractalqb/change"
)

var (
	_ json.Marshaler   = (*Obs[bool])(nil)
	_ json.Unmarshaler = (*Obs[bool])(nil)
)

func ExampleObs_json() {
	var cfg struct {
		Log Obs[bool]
	}
	cfg.Log.ObsRegister(0, nil, change.UpdateFunc(func(_ any, e change.Event) {
		ce := e.(change.Changed[bool])
		fmt.Printf("%t -> %t", ce.OldValue(), ce.NewValue())
	}))
	json.NewEncoder(os.Stdout).Encode(&cfg)
	json.Unmarshal([]byte(`{"Log": true}`), &cfg)
	// Output:
	// {"Log":false}
	// false -> true
}

type testFlagVal struct {
	i int
}

var _ flag.Value = (*testFlagVal)(nil)

func (v testFlagVal) String() string {
	return strconv.Itoa(v.i)
}
func (v *testFlagVal) Set(s string) (err error) {
	v.i, err = strconv.Atoi(s)
	return err
}

func TestObs_Flag_flagValue(t *testing.T) {
	var out strings.Builder
	o := NewObs(testFlagVal{}, nil, 1, change.UpdateFunc(func(_ any, e change.Event) {
		ce := e.(change.Changed[testFlagVal])
		fmt.Fprintf(&out, "%s -> %s", ce.OldValue(), ce.NewValue())
	}))
	if v := o.Get().i; v != 0 {
		t.Fatalf("Unexpected initial value: %d", v)
	}
	fs := flag.NewFlagSet(t.Name(), flag.ContinueOnError)
	o.Flag("n", "set a flag.Value", fs)
	err := fs.Parse([]string{"-n", "4"})
	if err != nil {
		t.Fatal(err)
	}
	if v := o.Get().i; v != 4 {
		t.Errorf("Unexpected value after change: %d", v)
	}
	if msg := out.String(); msg != "0 -> 4" {
		t.Errorf("Unexpected event output: '%s'", msg)
	}
}

func TestObs_Flag_duration(t *testing.T) {
	var out strings.Builder
	o := NewObs(time.Second, nil, 1, change.UpdateFunc(func(_ any, e change.Event) {
		ce := e.(change.Changed[time.Duration])
		fmt.Fprintf(&out, "%s -> %s", ce.OldValue(), ce.NewValue())
	}))
	if v := o.Get(); v != time.Second {
		t.Fatalf("Unexpected initial value: %d", v)
	}
	fs := flag.NewFlagSet(t.Name(), flag.ContinueOnError)
	o.Flag("d", "as a time.Duration", fs)
	err := fs.Parse([]string{"-d", "2m"})
	if err != nil {
		t.Fatal(err)
	}
	if v := o.Get(); v != 2*time.Minute {
		t.Errorf("Unexpected value after change: %s", v)
	}
	if msg := out.String(); msg != "1s -> 2m0s" {
		t.Errorf("Unexpected event output: '%s'", msg)
	}
}
