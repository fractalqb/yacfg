package yachg

import (
	"encoding/json"
	"flag"
	"sync"

	"git.fractalqb.de/fractalqb/daq"
	"github.com/fractalqb/change"
)

type On[T comparable] struct {
	change.On[T]
	mx sync.RWMutex
}

func NewOn[T comparable](init T, hook change.HookFunc[T]) On[T] {
	return On[T]{On: change.NewOn(init, hook)}
}

func (cv *On[T]) Get() T {
	cv.mx.RLock()
	defer cv.mx.RUnlock()
	return cv.On.Get()
}

func (cv *On[T]) Set(v T, chg change.Flags) change.Flags {
	cv.mx.Lock()
	defer cv.mx.Unlock()
	return cv.On.Set(v, chg)
}

func (o *On[T]) MarshalJSON() ([]byte, error) {
	v := o.Get()
	return json.Marshal(v)
}

func (o *On[T]) UnmarshalJSON(data []byte) error {
	var tmp T
	err := json.Unmarshal(data, &tmp)
	if err != nil {
		return err
	}
	o.Set(tmp, 0)
	return nil
}

func (o *On[T]) MergeDaQ(by *daq.Merge, add any) error {
	var tmp T
	if err := by.Merge(&tmp, add); err != nil {
		return err
	}
	o.Set(tmp, 0)
	return nil
}

func (o *On[T]) Flag(name, usage string, set *flag.FlagSet) {
	if set == nil {
		set = flag.CommandLine
	}
	set.Func(name, usage, func(s string) (err error) {
		var t T
		var ti any = &t
		if fval, ok := ti.(flag.Value); ok {
			fval.Set(s)
			(*On[T])(o).Set(*ti.(*T), 0)
			return nil
		}
		err = parse(s, &t)
		if err != nil {
			return err
		}
		(*On[T])(o).Set(t, 0)
		return nil
	})
}
