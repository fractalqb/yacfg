package yachg

import (
	"fmt"
	"strconv"
	"time"
)

func parse[T any](s string, res *T) (err error) {
	defer func() {
		if p := recover(); p != nil {
			err = fmt.Errorf("panic: %v", p)
		}
	}()
	var ri any = res
	switch ri := ri.(type) {
	case *string:
		*ri = s
	case *bool:
		*ri, err = strconv.ParseBool(s)
	case *int:
		*ri, err = strconv.Atoi(s)
	case *uint:
		var tmp uint64
		tmp, err = strconv.ParseUint(s, 10, 64)
		if err == nil {
			*ri = uint(tmp)
		}
	case *int64:
		*ri, err = strconv.ParseInt(s, 10, 64)
	case *uint64:
		*ri, err = strconv.ParseUint(s, 10, 64)
	case *float64:
		*ri, err = strconv.ParseFloat(s, 64)
	case *time.Duration:
		*ri, err = time.ParseDuration(s)
	default:
		err = fmt.Errorf("Cannot parse yachg flag type %T", res)
	}
	return err
}
