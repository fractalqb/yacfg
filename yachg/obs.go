package yachg

import (
	"encoding/json"
	"flag"
	"sync"

	"git.fractalqb.de/fractalqb/daq"
	"github.com/fractalqb/change"
)

type Obs[T comparable] struct {
	change.Obs[T]
	mx sync.RWMutex
}

func NewObs[T comparable](init T, defaultTag interface{}, defaultChg change.Flags, os ...change.Observer) Obs[T] {
	return Obs[T]{Obs: change.NewObs(init, defaultTag, defaultChg, os...)}
}

func (o *Obs[T]) Get() T {
	o.mx.RLock()
	defer o.mx.RUnlock()
	return o.Obs.Get()
}

func (o *Obs[T]) Set(v T, chg change.Flags) change.Flags {
	o.mx.Lock()
	defer o.mx.Unlock()
	return o.Obs.Set(v, chg)
}

func (o *Obs[T]) MarshalJSON() ([]byte, error) {
	v := o.Get()
	return json.Marshal(v)
}

func (o *Obs[T]) UnmarshalJSON(data []byte) error {
	var tmp T
	err := json.Unmarshal(data, &tmp)
	if err != nil {
		return err
	}
	o.Set(tmp, 0)
	return nil
}

func (o *Obs[T]) MergeDaQ(by *daq.Merge, add any) error {
	var tmp T
	if err := by.Merge(&tmp, add); err != nil {
		return err
	}
	o.Set(tmp, 0)
	return nil
}

func (o *Obs[T]) Flag(name, usage string, set *flag.FlagSet) {
	if set == nil {
		set = flag.CommandLine
	}
	set.Func(name, usage, func(s string) (err error) {
		var t T
		var ti any = &t
		if fval, ok := ti.(flag.Value); ok {
			fval.Set(s)
			(*Obs[T])(o).Set(*ti.(*T), 0) // TODO test if this works
			return nil
		}
		err = parse(s, &t)
		if err != nil {
			return err
		}
		(*Obs[T])(o).Set(t, 0)
		return nil
	})
}
