package yachg

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/fractalqb/change"
)

var (
	_ json.Marshaler   = (*On[bool])(nil)
	_ json.Unmarshaler = (*On[bool])(nil)
)

func testHook[T comparable](w io.Writer) func(*change.On[T], T, T, bool) change.Flags {
	return func(s *change.On[T], o, n T, check bool) change.Flags {
		if !check {
			fmt.Fprintf(w, "%v -> %v", o, n)
		}
		return 1
	}
}

func ExampleOn_json() {
	cfg := struct {
		Log On[bool]
	}{
		Log: NewOn(false, testHook[bool](os.Stdout)),
	}
	json.NewEncoder(os.Stdout).Encode(&cfg)
	json.Unmarshal([]byte(`{"Log": true}`), &cfg)
	// Output:
	// {"Log":false}
	// false -> true
}

func TestOn_Flag_flagValue(t *testing.T) {
	var out strings.Builder
	o := NewOn(testFlagVal{}, testHook[testFlagVal](&out))
	if v := o.Get().i; v != 0 {
		t.Fatalf("Unexpected initial value: %d", v)
	}
	fs := flag.NewFlagSet(t.Name(), flag.ContinueOnError)
	o.Flag("n", "set a flag.Value", fs)
	err := fs.Parse([]string{"-n", "4"})
	if err != nil {
		t.Fatal(err)
	}
	if v := o.Get().i; v != 4 {
		t.Errorf("Unexpected value after change: %d", v)
	}
	if msg := out.String(); msg != "0 -> 4" {
		t.Errorf("Unexpected event output: '%s'", msg)
	}
}

func TestOn_Flag_duration(t *testing.T) {
	var out strings.Builder
	o := NewOn(time.Second, testHook[time.Duration](&out))
	if v := o.Get(); v != time.Second {
		t.Fatalf("Unexpected initial value: %d", v)
	}
	fs := flag.NewFlagSet(t.Name(), flag.ContinueOnError)
	o.Flag("d", "as a time.Duration", fs)
	err := fs.Parse([]string{"-d", "2m"})
	if err != nil {
		t.Fatal(err)
	}
	if v := o.Get(); v != 2*time.Minute {
		t.Errorf("Unexpected value after change: %s", v)
	}
	if msg := out.String(); msg != "1s -> 2m0s" {
		t.Errorf("Unexpected event output: '%s'", msg)
	}
}
