package yachg

import "testing"

func TestParse(t *testing.T) {
	var v string
	err := parse("foo", &v)
	if err != nil {
		t.Error(err)
	}
	if v != "foo" {
		t.Errorf("Expect 'foo', got '%s'", v)
	}
}
