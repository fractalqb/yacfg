package yacfg

import (
	"fmt"
	"path/filepath"
)

const testFilesDir = "testdata"

func ExampleFlagFiles() {
	const lsSep = string(filepath.ListSeparator)

	c, err := FlagFiles([]string{"-cfg"}, "cfg", testFilesDir)
	fmt.Printf("'%s' (%v)\n", c, err)
	c, err = FlagFiles([]string{"--cfg"}, "cfg", testFilesDir)
	fmt.Printf("'%s' (%v)\n", c, err)
	c, err = FlagFiles([]string{"--c"}, "cfg", testFilesDir)
	fmt.Printf("'%s' (%v)\n", c, err)
	c, err = FlagFiles([]string{"--c"}, "c", testFilesDir)
	fmt.Printf("'%s' (%v)\n", c, err)
	c, err = FlagFiles([]string{"-c"}, "c", testFilesDir)
	fmt.Printf("'%s' (%v)\n", c, err)
	c, err = FlagFiles([]string{"-cfg", "cfg.json"}, "cfg", testFilesDir)
	fmt.Printf("'%s' (%v)\n", c, err)
	c, err = FlagFiles([]string{"-cfg=cfg.json"}, "cfg", testFilesDir)
	fmt.Printf("'%s' (%v)\n", c, err)
	c, err = FlagFiles([]string{"-cfg=no.json" + lsSep}, "cfg", testFilesDir)
	fmt.Printf("'%s' (%v)\n", c, err)
	c, err = FlagFiles([]string{"-cfg=no.json" + lsSep + "cfg.json"}, "cfg", testFilesDir)
	fmt.Printf("'%s' (%v)\n", c, err)
	c, err = FlagFiles([]string{
		"-cfg=no.json" + lsSep, // Finds nothing; no error because trailing ListSeparator
		"-cfg=no.json" + lsSep + "cfg.json" + lsSep + "cfg2.json", // Stop when finding cfg.json
		"-cfg=cfg2.json", // Adds cfg2.json
	}, "cfg", testFilesDir)
	fmt.Printf("'%s' (%v)\n", c, err)

	// Output:
	// '[]' (missing value for config flag 'cfg')
	// '[]' (missing value for config flag 'cfg')
	// '[]' (<nil>)
	// '[]' (missing value for config flag 'c')
	// '[]' (missing value for config flag 'c')
	// '[testdata/cfg.json]' (<nil>)
	// '[testdata/cfg.json]' (<nil>)
	// '[]' (<nil>)
	// '[testdata/cfg.json]' (<nil>)
	// '[testdata/cfg.json testdata/cfg2.json]' (<nil>)
}
