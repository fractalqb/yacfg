package yasec

import (
	"fmt"
	"net"
	"os"

	"github.com/awnumar/memguard"
)

// PasswordPrompt is a [Passworder] that reads a password directly from a
// terminal without echoing the user input. One can use e.g. [term].IsTerminal()
// to check if password can be provided this way.
//
// [term]: https://pkg.go.dev/golang.org/x/term
type PasswordPrompt string

var _ Passworder = PasswordPrompt("")

func (pw PasswordPrompt) Password() (*memguard.Enclave, error) {
	return PromptPassword(string(pw))
}

// UnixSocketPassword is a [Passworder] that reads the password from a temporary
// unix domain socket that is closed and deleted after the password has been
// read. Note that the unix domain socket can be intercepted by other processes
// which also it vulnerable for a short period of time.
//
// For example, if you start a programm with nohup, you can use this method to
// provide the password without including it in argv[] or envp[] (C style). Both
// are considered insecure.
type UnixSocketPassword string

var _ Passworder = UnixSocketPassword("")

func (pw UnixSocketPassword) Password() (*memguard.Enclave, error) {
	sock, err := net.Listen("unix", string(pw))
	if err != nil {
		return nil, err
	}
	defer func() {
		sock.Close()
		os.Remove(string(pw))
	}()
	conn, err := sock.Accept()
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	rpw := memguard.NewStream()
	if err := readPasswordLine(rpw, conn); err != nil {
		return nil, err
	}
	buf, err := rpw.Flush()
	if err != nil {
		return nil, err
	}
	return buf.Seal(), nil
}

type FDPassword uintptr

func (pw FDPassword) Password() (*memguard.Enclave, error) {
	r := os.NewFile(uintptr(pw), "password.yasec")
	if r == nil {
		return nil, fmt.Errorf("invalid file descriptor %d", pw)
	}
	rpw := memguard.NewStream()
	if err := readPasswordLine(rpw, r); err != nil {
		return nil, err
	}
	buf, err := rpw.Flush()
	if err != nil {
		return nil, err
	}
	return buf.Seal(), nil
}
