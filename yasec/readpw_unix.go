//go:build aix || darwin || dragonfly || freebsd || linux || netbsd || openbsd || solaris || zos

package yasec

import (
	"io"

	"golang.org/x/sys/unix"
)

// Significant parts copied from golang.org/x/term package

func readPassword(w io.Writer, fd int) error {
	termios, err := unix.IoctlGetTermios(fd, unix.TCGETS)
	if err != nil {
		return err
	}

	newState := *termios
	newState.Lflag &^= unix.ECHO
	newState.Lflag |= unix.ICANON | unix.ISIG
	newState.Iflag |= unix.ICRNL
	if err := unix.IoctlSetTermios(fd, unix.TCSETS, &newState); err != nil {
		return err
	}

	defer unix.IoctlSetTermios(fd, unix.TCSETS, termios)

	return readPasswordLine(w, passwordReader(fd))
}

type passwordReader int

func (r passwordReader) Read(buf []byte) (int, error) {
	return unix.Read(int(r), buf)
}
