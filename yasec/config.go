package yasec

import (
	"sync"
	"unsafe"

	"github.com/awnumar/memguard"
	"golang.org/x/crypto/argon2"
)

// Config holds the master password that is used to decrypt a [Secret] when it
// opened for actual use.
type Config struct {
	GetPassword Passworder
	Salt        []byte

	key  *memguard.Enclave
	lock sync.RWMutex
}

var (
	// DefaultConfig config is used to access a [Secret] when that secret has no
	// explicit Config.
	DefaultConfig Config

	// The default salt used when calling the KDF for [Config] password.
	DefaultSalt []byte
)

func NewConfig(key *memguard.Enclave) Config { return Config{key: key} }

func (cfg *Config) Set(key *memguard.Enclave) {
	cfg.lock.Lock()
	cfg.key = key
	cfg.lock.Unlock()
}

func (cfg *Config) SetPassword(passwd, salt []byte) {
	cfg.lock.Lock()
	cfg.setPassword(passwd, salt)
	cfg.lock.Unlock()
}

func (cfg *Config) setPassword(passwd, salt []byte) {
	if len(passwd) == 0 {
		cfg.key = nil
	} else {
		key := kdf(passwd, salt)
		cfg.key = memguard.NewBufferFromBytes(key).Seal()
	}
}

func (cfg *Config) SetPasswordString(passwd string, salt []byte) {
	cfg.lock.Lock()
	// Do not copy data around unnecessarily
	var pw []byte = unsafe.Slice(unsafe.StringData(passwd), len(passwd))
	cfg.setPassword(pw, salt)
	cfg.lock.Unlock()
}

// Passworders are used to supply a master password for [Config] (see also
// [Config.SetPasswordFrom]).
type Passworder interface {
	Password() (*memguard.Enclave, error)
}

type PasswordFunc func() (*memguard.Enclave, error)

func (f PasswordFunc) Password() (*memguard.Enclave, error) { return f() }

func (cfg *Config) SetPasswordFrom(pwr Passworder, salt []byte) error {
	cfg.lock.Lock()
	defer cfg.lock.Unlock()
	return cfg.setPasswordFrom(pwr, salt)
}

func (cfg *Config) setPasswordFrom(pwr Passworder, salt []byte) error {
	pw, err := pwr.Password()
	if err != nil {
		return err
	}
	buf, err := pw.Open()
	if err != nil {
		return err
	}
	defer buf.Destroy()
	cfg.setPassword(buf.Bytes(), salt)
	return nil
}

func (cfg *Config) SetFromPrompt(text string, salt []byte) error {
	return cfg.setPasswordFrom(PasswordPrompt(text), salt)
}

func (cfg *Config) SetFromUnixSocket(path string, salt []byte) error {
	return cfg.setPasswordFrom(UnixSocketPassword(path), salt)
}

func kdf(pw, salt []byte) []byte {
	if len(salt) == 0 {
		salt = DefaultSalt
	}
	return argon2.IDKey(pw, salt, 1, 64*1024, 4, 32)
}

func (cfg *Config) encrypt(cypher, plain []byte) ([]byte, error) {
	keyBuf, err := cfg.getKey().Open()
	if err != nil {
		return nil, err
	}
	defer keyBuf.Destroy()
	return xencrypt(keyBuf.Bytes(), cypher, plain)
}

func (cfg *Config) decrypt(plain, cypher []byte) (int, error) {
	keyBuf, err := cfg.getKey().Open()
	if err != nil {
		return 0, err
	}
	defer keyBuf.Destroy()
	return xdecrypt(keyBuf.Bytes(), plain, cypher)
}

func (cfg *Config) getKey() *memguard.Enclave {
	cfg.lock.RLock()
	defer cfg.lock.RUnlock()
	if cfg.key == nil && cfg.GetPassword != nil {
		cfg.setPasswordFrom(cfg.GetPassword, cfg.Salt)
	}
	return cfg.key
}
