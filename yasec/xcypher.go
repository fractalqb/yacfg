package yasec

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"slices"

	"github.com/awnumar/memguard"
)

func xencrypt(key, cypher, plain []byte) ([]byte, error) {
	cypher = binary.AppendVarint(cypher, int64(defaultXcyper))
	xc := xcyphers[defaultXcyper]
	return xc.encrypt(key, cypher, plain)
}

func xdecrypt(key, plain, cypher []byte) (int, error) {
	xcid, xcidLen := binary.Varint(cypher)
	if xcidLen <= 0 {
		return 0, errors.New("cannot decode xcypher ID")
	}
	if xcid < 0 || xcid >= int64(len(xcyphers)) {
		return 0, fmt.Errorf("illegal xcypher ID %d", xcid)
	}
	xc := xcyphers[xcid]
	return xc.decrypt(key, plain, cypher[xcidLen:])
}

var xcyphers = []xcypher{
	aes_gcm{},
}

const defaultXcyper = 0

type xcypher interface {
	encrypt(key, cypher, plain []byte) ([]byte, error)
	decrypt(key, plain, cypher []byte) (int, error)
}

type aes_gcm struct{}

func (aes_gcm) encrypt(key, cypher, plain []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	xcidLen := len(cypher)
	cypher = slices.Grow(cypher, aesgcm.NonceSize())[:xcidLen+aesgcm.NonceSize()]
	if _, err := io.ReadFull(rand.Reader, cypher[xcidLen:]); err != nil {
		return cypher, err
	}
	cypher = aesgcm.Seal(cypher, cypher[xcidLen:], plain, nil)
	return cypher, nil
}

func (aes_gcm) decrypt(key, plain, cypher []byte) (int, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return 0, err
	}
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return 0, err
	}
	nonce := cypher[:aesgcm.NonceSize()]
	cypher = cypher[aesgcm.NonceSize():]
	tmp, err := aesgcm.Open(plain[:0], nonce, cypher, nil)
	if err != nil {
		return 0, fmt.Errorf("yasec config decrypt: %w", err)
	}
	if &tmp[0] != &plain[0] { // Unexpected reallocation of plaintext buffer
		tmp = tmp[:cap(tmp)]
		clear(tmp)
		memguard.SafePanic("yasec: realloc when decrypting secret")
	}
	return len(tmp), nil
}
