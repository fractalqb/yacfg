package yasec

import (
	"fmt"
	"io"
	"os"
	"runtime"

	"github.com/awnumar/memguard"
)

type PasswordsDiffer struct{}

func (PasswordsDiffer) Error() string { return "passwords differ" }

func PromptNewPassword(p1, p2 string) (*memguard.Enclave, error) {
	pw1, err := PromptPassword(p1)
	if err != nil {
		return nil, err
	}
	pw2, err := PromptPassword(p2)
	if err != nil {
		return nil, err
	}
	pw1c, err := pw1.Open()
	if err != nil {
		return nil, err
	}
	defer pw1c.Destroy()
	pw2c, err := pw2.Open()
	if err != nil {
		return nil, err
	}
	defer pw2c.Destroy()
	if !pw1c.EqualTo(pw2c.Bytes()) {
		return nil, PasswordsDiffer{}
	}
	return pw1, nil
}

func PromptPassword(p string) (*memguard.Enclave, error) {
	fmt.Print(p)
	enc, err := ReadPassword()
	fmt.Println()
	return enc, err
}

func ReadPassword() (*memguard.Enclave, error) {
	s := memguard.NewStream()
	if err := readPassword(s, int(os.Stdin.Fd())); err != nil {
		return nil, err
	}
	buf, err := s.Flush()
	if err != nil {
		return nil, err
	}
	return buf.Seal(), nil
}

func readPasswordLine(wr io.Writer, reader io.Reader) error {
	var buf [1]byte
	defer clear(buf[:])

	for {
		n, err := reader.Read(buf[:])
		if n > 0 {
			switch buf[0] {
			// TODO is the a reasonable way to support backspace?
			// case '\b':
			// 	if len(ret) > 0 {
			// 		ret = ret[:len(ret)-1]
			// 	}
			case '\n':
				if runtime.GOOS != "windows" {
					return nil
				}
				// otherwise ignore \n
			case '\r':
				if runtime.GOOS == "windows" {
					return nil
				}
				// otherwise ignore \r
			default:
				_, err = wr.Write(buf[:])
				clear(buf[:])
				if err != nil {
					return err
				}
			}
			continue
		}
		if err != nil {
			if err == io.EOF {
				return nil
			}
			return err
		}
	}
}
