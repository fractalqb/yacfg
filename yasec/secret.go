// Package yasec helps to keep secrets in config files as secret as possible.
// (Needs to be audited)
package yasec

import (
	"bytes"
	"encoding/base64"
	"errors"
	"slices"

	"git.fractalqb.de/fractalqb/daq"
	"github.com/awnumar/memguard"
)

// Age key fit pretty well (https://github.com/FiloSottile/age)

type Secret struct {
	Config *Config
	data   []byte
}

func (sec Secret) Empty() bool { return sec.data == nil }

func (sec Secret) Protected() bool {
	cfg := sec.config()
	cfg.lock.RLock()
	ok := sec.config().key != nil
	cfg.lock.RUnlock()
	return ok
}

func (sec *Secret) Set(buf *memguard.LockedBuffer) (err error) {
	if !sec.Protected() {
		return errors.New("cannot set unprotected secret")
	}
	clear(sec.data[:cap(sec.data)])
	sec.data, err = sec.config().encrypt(sec.data[:0], buf.Bytes())
	return err
}

type EmptySecret struct{}

func (EmptySecret) Error() string { return "empty secret" }

func (sec Secret) Open() (*memguard.LockedBuffer, error) {
	if sec.Empty() {
		return nil, EmptySecret{}
	}
	cfg := sec.config()
	if !sec.Protected() {
		if cfg.GetPassword == nil {
			return nil, errors.New("cannot open unprotected secret")
		}
		err := cfg.setPasswordFrom(cfg.GetPassword, cfg.Salt)
		if err != nil {
			return nil, err
		}
	}
	tmp := memguard.NewBuffer(len(sec.data))
	sz, err := sec.config().decrypt(tmp.Bytes(), sec.data)
	if err != nil {
		tmp.Destroy()
		return nil, err
	}
	res := memguard.NewBuffer(sz)
	res.Copy(tmp.Bytes())
	tmp.Destroy()
	res.Freeze()
	return res, nil
}

func (sec Secret) Equal(data []byte) (bool, error) {
	buf, err := sec.Open()
	if err != nil {
		return false, err
	}
	defer buf.Destroy()
	return buf.EqualTo(data), nil
}

func (sec Secret) EqualString(s string) (bool, error) {
	return sec.Equal([]byte(s))
}

func (sec Secret) MarshalBinary() (data []byte, err error) {
	return sec.data, nil
}

func (sec *Secret) UnmarshalBinary(data []byte) error {
	sec.data = bytes.Clone(data) // TODO clone?
	return nil
}

var b64enc = base64.RawStdEncoding

func (sec Secret) MarshalText() (text []byte, err error) {
	text = make([]byte, b64enc.EncodedLen(len(sec.data)))
	base64.RawStdEncoding.Encode(text, sec.data)
	return text, nil
}

func (sec *Secret) UnmarshalText(text []byte) error {
	l := b64enc.DecodedLen(len(text))
	sec.data = slices.Grow(sec.data, l)[:l]
	_, err := b64enc.Decode(sec.data, text)
	return err
}

func (sec *Secret) MergeDaQ(m *daq.Merge, add any) error {
	var txt string
	if err := m.Merge(&txt, add); err != nil {
		return err
	}
	return sec.UnmarshalText([]byte(txt))
}

func (sec *Secret) config() *Config {
	if sec.Config == nil {
		return &DefaultConfig
	}
	return sec.Config
}
