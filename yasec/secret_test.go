package yasec

import (
	"fmt"
	"strings"

	"git.fractalqb.de/fractalqb/eloc/must"
	"github.com/awnumar/memguard"
)

func ExampleSecret() {
	key := memguard.NewBufferFromBytes([]byte{
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
	})
	cfg := NewConfig(key.Seal())
	secret := Secret{Config: &cfg}
	data := must.Ret(memguard.NewBufferFromEntireReader(strings.NewReader("foo bar baz")))
	secret.Set(data)
	data.Destroy()

	data = must.Ret(secret.Open())
	defer data.Destroy()
	fmt.Println(data.String())
	// Output:
	// foo bar baz
}
