package yacfg

import (
	"flag"
	"os"
)

type FromEnvThenFiles struct {
	EnvPrefix     string
	FilesFlagName string
	Flags         *flag.FlagSet
	Log           func(string)
}

func (cfgr FromEnvThenFiles) Configure(cfg any) (files []string, err error) {
	if cfgr.FilesFlagName != "" {
		flags := flag.CommandLine
		if cfgr.Flags != nil {
			flags = cfgr.Flags
		}
		flags.Func(
			cfgr.FilesFlagName,
			FlagFilesDoc(cfgr.FilesFlagName),
			func(c string) error { /* do nothing */ return nil },
		)
	}

	envMap := EnvMapList{
		EnvMapTag(true),
		EnvMapUpper{EnvMapField{Prefix: cfgr.EnvPrefix, PathSep: "_"}},
	}
	if err = Env(cfg, envMap); err != nil {
		return nil, err
	}

	cfgFiles, err := FlagFiles(os.Args[1:], cfgr.FilesFlagName, ".")
	if err != nil {
		return nil, err
	}
	return Files(cfg, cfgr.Log, cfgFiles...)
}

func (cfgr FromEnvThenFiles) MustConfigure(cfg any) (files []string) {
	var err error
	files, err = cfgr.Configure(cfg)
	if err != nil {
		panic(err)
	}
	return files
}

func (cfgr FromEnvThenFiles) ListFlagDoc() string {
	return FlagFilesDoc(cfgr.FilesFlagName)
}
