module git.fractalqb.de/fractalqb/yacfg

go 1.22.0

toolchain go1.23.2

require (
	git.fractalqb.de/fractalqb/daq v0.11.0
	github.com/fractalqb/change v0.8.0
	github.com/rjeczalik/notify v0.9.3
	golang.org/x/crypto v0.31.0
	golang.org/x/term v0.27.0
	gopkg.in/yaml.v3 v3.0.1
)

require github.com/awnumar/memcall v0.4.0 // indirect

require (
	git.fractalqb.de/fractalqb/eloc v0.3.0
	github.com/awnumar/memguard v0.22.5
	github.com/kr/pretty v0.3.0 // indirect
	golang.org/x/sys v0.28.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
