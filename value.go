package yacfg

import (
	"fmt"
	"reflect"
	"strconv"
	"time"
)

// ValueSetter is an interface that allows to define how a value is parsed from
// string.
type ValueSetter interface {
	Set(string) error
}

func setValue(v reflect.Value, str string) error {
	if vs, ok := v.Interface().(ValueSetter); ok {
		return vs.Set(str)
	}
	switch v.Kind() {
	case reflect.Bool:
		envVal, err := strconv.ParseBool(str)
		if err != nil {
			return err
		}
		v.Set(reflect.ValueOf(envVal))
		return nil
	case reflect.String:
		v.Set(reflect.ValueOf(str))
		return nil
	case reflect.Int:
		envVal, err := strconv.Atoi(str)
		if err != nil {
			return err
		}
		v.Set(reflect.ValueOf(envVal))
		return nil
	case reflect.Float64:
		envVal, err := strconv.ParseFloat(str, 64)
		if err != nil {
			return err
		}
		v.Set(reflect.ValueOf(envVal))
		return nil
	case reflect.Uint64:
		envVal, err := strconv.ParseUint(str, 10, 64)
		if err != nil {
			return err
		}
		v.Set(reflect.ValueOf(envVal))
		return nil
	}
	switch v.Interface().(type) {
	case time.Duration:
		envVal, err := time.ParseDuration(str)
		if err != nil {
			return err
		}
		v.Set(reflect.ValueOf(envVal))
	default:
		return fmt.Errorf("unsupported target type %T", v.Interface())
	}
	return nil
}
