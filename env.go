package yacfg

import (
	"errors"
	"fmt"
	"os"
	paths "path"
	"reflect"
	"strings"
)

// EnvMapper is an interface that defines how the name of the
// environment variable is determined depending on the configuration
// field that is to be set.
//
// See also: Env
type EnvMapper interface {
	// VarName computes the name of the environment variable from the
	// current field of the config structure and the path, i.e. the
	// names of all parent structures, of the filed. If no environment
	// variable can be determined name is the empty string.
	//
	// When an EnvMapper is used in an EnvMapList the returned stop
	// value determines if further mappers are queried if name is
	// empty.
	VarName(f *reflect.StructField, path []string) (name string, stop bool)
}

// DefaultEnvMapper first tries to determine the name of an
// environment variable from the field tag with the key 'env'—stopping
// when the value is "-". Next it takes the field's path and name
// joined with "_" as separator and converts the result to upper case.
var DefaultEnvMapper = EnvMapList{
	EnvMapTag(true),
	EnvMapUpper{EnvMapField{PathSep: "_"}},
}

// EnvMapList successively tries the listed EnvMappers until it finds
// a non-empty name for an environment variable. If one EnvMapper
// returns stop==true EnvMapList immediatley returns the result from
// that mapper.
type EnvMapList []EnvMapper

// VarName implements EnvMapper
func (m EnvMapList) VarName(f *reflect.StructField, path []string) (string, bool) {
	for _, em := range m {
		nm, stop := em.VarName(f, path)
		if nm != "" || stop {
			return nm, stop
		}
	}
	return "", false
}

// EnvMap explicitly maps the complete path of a config field to the
// name of an environment variable. The complete path is the joined
// path with the field's name computed by path.Join().
type EnvMap map[string]string

// VarName implements EnvMapper
func (m EnvMap) VarName(f *reflect.StructField, path []string) (string, bool) {
	p := paths.Join(append(path, f.Name)...)
	return m[p], false
}

// EnvMapTag inspects the field's tag with the key 'env' for the name
// of the environment variable. EnvMapTag(true) return stop==true for
// the tag `env:"-"`. I.e. one can specify a struct field to be not
// intended for Env configuration. With EnvMapTag(false) that intend
// can be ignored.
type EnvMapTag bool

// VarName implements EnvMapper
func (stop EnvMapTag) VarName(f *reflect.StructField, path []string) (string, bool) {
	tag, ok := f.Tag.Lookup("env")
	if !ok {
		return "", false
	}
	if tag == "-" {
		return "", bool(stop)
	}
	return tag, false
}

// EnvMapUpper wraps another EnvMapper and converts its result to
// upper case.
type EnvMapUpper struct {
	Map EnvMapper
}

// VarName implements EnvMapper
func (em EnvMapUpper) VarName(f *reflect.StructField, path []string) (string, bool) {
	nm, stop := em.Map.VarName(f, path)
	return strings.ToUpper(nm), stop
}

// EnvMapField computes the name of the environment variable from the
// config field.  The name will start with Prefix and ends with the
// field name.
//
// If PathSep is not empty the elements of the field's path with
// appended separator will be put between the prefix and the field
// name.
type EnvMapField struct {
	Prefix  string
	PathSep string
}

// VarName implements EnvMapper
func (em EnvMapField) VarName(f *reflect.StructField, path []string) (string, bool) {
	var sb strings.Builder
	sb.WriteString(em.Prefix)
	if em.PathSep != "" {
		for _, p := range path {
			sb.WriteString(p)
			sb.WriteString(em.PathSep)
		}
	}
	sb.WriteString(f.Name)
	return sb.String(), false
}

type EnvMapUpTo struct {
	Level int
	Map   EnvMapper
}

// VarName implements EnvMapper
func (em EnvMapUpTo) VarName(f *reflect.StructField, path []string) (string, bool) {
	if len(path) > em.Level {
		return "", false
	}
	return em.Map.VarName(f, path)
}

// Env visits all fields of the cfg struct and uses the EvnMapper e to
// find an environment variable that provides a value for the
// field. If a value can be obtained from the environment is is set to
// the field in cfg.
func Env(cfg any, m EnvMapper) error {
	val := reflect.ValueOf(cfg)
	if val.Kind() != reflect.Ptr {
		return errors.New("cfg is not a pointer")
	}
	return fromEnv(val, m, nil)
}

// DefaultEnv calls Env with DefaultEnvMapper
func DefaultEnv(cfg any) error {
	return Env(cfg, DefaultEnvMapper)
}

func fromEnv(cfg reflect.Value, m EnvMapper, p []string) error {
	cfg = reflect.Indirect(cfg)
	typ := cfg.Type()
	fnum := typ.NumField()
	for i := 0; i < fnum; i++ {
		ft := typ.Field(i)
		fv := cfg.Field(i)
		switch {
		case ft.Anonymous:
			if err := fromEnv(fv, m, p); err != nil {
				return err
			}
		case fv.Kind() == reflect.Struct:
			if err := fromEnv(fv, m, append(p, ft.Name)); err != nil {
				return err
			}
		case fv.Kind() == reflect.Interface:
			fv = reflect.Indirect(fv.Elem())
			if fv.Kind() == reflect.Struct {
				if err := fromEnv(fv, m, append(p, ft.Name)); err != nil {
					return err
				}
			} else if err := setVal(fv, &ft, m, p); err != nil {
				return fmt.Errorf("error setting %s/%s: %s", p, ft.Name, err)
			}
		default:
			err := setVal(fv, &ft, m, p)
			if err != nil {
				return fmt.Errorf("error setting %s/%s: %s", p, ft.Name, err)
			}
		}
	}
	return nil
}

func setVal(v reflect.Value, f *reflect.StructField, m EnvMapper, p []string) error {
	env, _ := m.VarName(f, p)
	if env == "" {
		return nil
	}
	envStr, ok := os.LookupEnv(env)
	if !ok {
		return nil
	}
	return setValue(v, envStr)
}
