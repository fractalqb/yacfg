package yacfg

import (
	"fmt"
	"os"
)

func ExampleDefaultEnv() {
	type testCfgBase struct {
		Foo string `env:"HOME"`
		Bar int
	}
	type testCfg struct {
		testCfgBase
		Baz  bool
		Quux testCfgBase
		Home string
		Bar  int `env:"-"`
	}

	os.Setenv("HOME", "/usr/johndoe")
	os.Setenv("BAR", "4")
	os.Setenv("QUUX_BAR", "4711")
	os.Setenv("BAZ", "true")
	var cfg testCfg
	fmt.Println(DefaultEnv(&cfg))
	fmt.Println(cfg)
	// Output:
	// <nil>
	// {{/usr/johndoe 4} true {/usr/johndoe 4711} /usr/johndoe 0}
}

func ExampleEnv() {
	type testCfgBase struct {
		Foo string `env:"HOME"`
		Bar int
	}
	type testCfg struct {
		testCfgBase
		Baz  bool
		Quux testCfgBase
		Home string
		Bar  int `env:"-"`
	}

	os.Setenv("HOME", "/usr/johndoe")
	os.Setenv("YACFG_HOME", "/usr/johndoe")
	os.Setenv("YACFG_BAR", "4")
	os.Setenv("QUUX_BAR", "4711")
	os.Setenv("FLAG", "true")
	var cfg testCfg
	fmt.Println(Env(&cfg, EnvMapList{
		EnvMap{"Baz": "FLAG", "Quux/Bar": "QUUX_BAR"},
		EnvMapTag(true),
		EnvMapUpper{EnvMapField{Prefix: "YACFG_"}},
	}))
	fmt.Println(cfg)
	// Output:
	// <nil>
	// {{/usr/johndoe 4} true {/usr/johndoe 4711} /usr/johndoe 0}
}
