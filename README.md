# yacfg – Yet Another Config Library (for Go)
[![Go Reference](https://pkg.go.dev/badge/git.fractalqb.de/fractalqb/yacfg.svg)](https://pkg.go.dev/git.fractalqb.de/fractalqb/yacfg)

Package yacfg is yet another config package. 

- Sets configuration values from environment
- Loads more than one config file and merges them (last write wins)
- Works with Go's standard `flag` library
- Can watch config files for changes and react to config changes
- Can protect secrets using [memguard](https://github.com/awnumar/memguard)

``` Go
var config = struct {
	Log    yachg.Obs[string] // Observable string
	SubPkg subpkg.Config
}{
	Log: yachg.NewObs("info"),
	SubPkg: subpkg.Config{
		SrvAddr: "localhost:8080",
		Timeout: 5 * time.Second,
	},
}

func init() {
	// Observe config.Log to change log level, when config changes
	cfg.LogLevel.ObsRegister(0, "", change.UpdateFunc(func(_ any, e change.Event) {
		ce := e.(change.Changed[string])
		log.Printf("Reconfigure log-level to %s", ce.NewValue())
		log.SetLogLevel(...)
	}))
}

func main() {
	cfgFiles, _ := yacfg.EnvThenFiles{
		EnvPrefix:     "YACFG_", // E.g. export YACFG_SUBPKG_TIMEOUT=3s
		FilesFlagName: "config", // Use '-config' (--config) on CLI to load files
	}.Configure(&cfg)            // Now, evaluate env and load files
	
	// Now the flags…
	flag.StringVar(&config.SubPkg.SrvAddr, "srv-addr", config.SubPkg.SrvAddr,
		"Set server address")
	flag.DurationVar(&config.SubPkg.Timeout, "srv-timeout", config.SubPkg.Timeout,
		"Set server timeout duration")
	flag.StringVar(&config.Log, "log", config.Log,
		"Set log level")
	flag.Parse()

	go yachg.Reload(&cfg, nil, cfgFiles...) // Start watching config files
	...
}
```