package yacfg

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"git.fractalqb.de/fractalqb/daq"
)

func FlagFilesDoc(name string) string {
	return fmt.Sprintf(
		`File names separated by '%[1]c' are checked until the first file is found.
An error occurs if none of the files can be read. Put '%[1]c' at the end of a
flag value to avoid this error. To read more than one file, '%[2]s' is specified
repeatedly.`,
		filepath.ListSeparator,
		name,
	)
}

func GlobFlagDoc() string {
	return fmt.Sprintf(
		`Set list of file glob pattern used to find configuration files.
Patterns are separatedy by '%c'.`,
		filepath.ListSeparator,
	)
}

func MustFlagFiles(args []string, flag, dir string) []string {
	files, err := FlagFiles(args, flag, dir)
	if err != nil {
		panic(err)
	}
	return files
}

// FlagFiles looks up all config files from flags named flag in args.
func FlagFiles(args []string, flag, dir string) (files []string, err error) {
	switch {
	case flag == "":
		return nil, nil
	case strings.ContainsRune(flag, '='):
		return nil, fmt.Errorf("flag name '%s' contains '='", flag)
	}
	longa := "--" + flag + "="
	shorta := longa[1:]
	long := longa[:len(longa)-1]
	short := long[1:]
	isCfgFlag := func(arg string) (ok bool, rest int) {
		switch arg {
		case short, long:
			return true, -1
		}
		switch {
		case strings.HasPrefix(arg, shorta):
			return true, len(shorta)
		case strings.HasPrefix(arg, longa):
			return true, len(longa)
		}
		return false, -1
	}
	for i, arg := range args {
		isCfg, rest := isCfgFlag(arg)
		if !isCfg {
			continue
		}
		var val string
		switch {
		case rest < 0:
			i++
			if i >= len(args) {
				return nil, fmt.Errorf("missing value for config flag '%s'", flag)
			}
			val = args[i]
		case rest < len(arg):
			val = arg[rest:]
		default:
			return nil, fmt.Errorf("missing value for config flag '%s'", flag)
		}
		names := filepath.SplitList(val)
		found := false
		for i, n := range names {
			if n == "" && i+1 == len(names) {
				continue
			}
			if !filepath.IsAbs(n) {
				n = filepath.Join(dir, n)
			}
			if _, err := os.Stat(n); err != nil {
				if errors.Is(err, os.ErrNotExist) {
					continue
				}
				return files, err
			}
			found = true
			files = append(files, n)
			break
		}
		if !found && names[len(names)-1] != "" {
			return files, fmt.Errorf("missing config file for '-%s=%s'", flag, val)
		}
	}
	return files, nil
}

// Files loads all config files in names into the configuration cfg and returns
// the names of all read files. Configration values are subsequently merged into
// cfg using [daq Merge].
//
// [daq Merge]: https://pkg.go.dev/git.fractalqb.de/fractalqb/daq#Merge
func Files(cfg any, log func(string), names ...string) (files []string, err error) {
	for _, nm := range names {
		ext := filepath.Ext(nm)
		if ext == "" {
			return files, fmt.Errorf("config file without extension: '%s'", nm)
		}
		rdfunc := cfgFileRd[ext]
		if rdfunc == nil {
			return files, fmt.Errorf("unsupported config file format: '%s'", nm)
		}
		rd, err := os.Open(nm)
		if err != nil {
			return files, err
		}
		err = rdfunc(rd, cfg)
		rd.Close()
		if err != nil {
			return files, fmt.Errorf("config file '%s': %w", nm, err)
		}
		if log != nil {
			log(fmt.Sprintf("read config '%s'", nm))
		}
		files = append(files, nm)
	}
	return files, nil
}

// FileReadFunc reads config data from files of a specific format,
// e.g. JSON.
type FileReadFunc = func(rd io.Reader, cfg any) error

// FileFormat registers a FileReadFunc for the file extension ext. The
// Go JSON reader from "encoding/json" is preconfigured for the
// ".json" extension.
func FileFormat(ext string, f FileReadFunc) {
	cfgFileRd[ext] = f
}

var cfgFileRd = map[string]FileReadFunc{
	".json": readFile,
}

func readFile(rd io.Reader, cfg any) error {
	tmp := make(map[string]any)
	if err := json.NewDecoder(rd).Decode(&tmp); err != nil {
		return err
	}
	return daq.DefaultMerge.Merge(cfg, tmp)
}
