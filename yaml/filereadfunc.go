// Package yaml registers a YAML config file reader for the extensions ".yaml"
// and ".yml".
//
// Use it as:
//
//	import _ "git.fractalqb.de/fractalqb/yacfg/yaml"
package yaml

import (
	"io"

	"git.fractalqb.de/fractalqb/daq"
	"git.fractalqb.de/fractalqb/yacfg"
	"gopkg.in/yaml.v3"
)

func init() {
	yacfg.FileFormat(".yaml", ReadFile)
	yacfg.FileFormat(".yml", ReadFile)
}

func ReadFile(rd io.Reader, cfg any) error {
	tmp := make(map[string]any)
	err := yaml.NewDecoder(rd).Decode(&tmp)
	if err != nil {
		return err
	}
	return daq.DefaultMerge.Merge(cfg, tmp)
}
