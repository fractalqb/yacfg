// Command yasec manages secrets for your config files.
package main

import (
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"

	"git.fractalqb.de/fractalqb/yacfg/yasec"
	"github.com/awnumar/memguard"
	"golang.org/x/term"
	"gopkg.in/yaml.v3"
)

var cfg struct {
	Salt    string
	format  string
	sendPW  bool
	decrypt bool
}

func usage() {
	w := flag.CommandLine.Output()
	fmt.Fprintln(w,
		`Usage: yasec [-d] [-f json|yaml] <secret>... | -p <mode> <args>...`,
	)
	flag.PrintDefaults()
}

func flags() {
	flag.Usage = usage
	flag.StringVar(&cfg.Salt, "salt", cfg.Salt,
		"Set salt for master password as hex string. Overrides env YASEC_SALT.",
	)
	flag.StringVar(&cfg.format, "f", cfg.format,
		"Format output: json, yaml",
	)
	flag.BoolVar(&cfg.sendPW, "p", cfg.sendPW,
		`Read master password from terminal an send via <mode>
- unix <socket name>...`,
	)
	flag.BoolVar(&cfg.decrypt, "d", cfg.decrypt,
		"Decrypt secrets and print plaintext.",
	)
	flag.Parse()
}

func main() {
	flags()

	if cfg.sendPW {
		sendPassword(flag.Args())
		return
	}

	must(yasec.DefaultConfig.SetFromPrompt("passphrase:", salt()))

	if cfg.decrypt {
		for _, arg := range flag.Args() {
			decrypt(arg)
		}
	} else if flag.NArg() > 0 {
		for _, arg := range flag.Args() {
			encrypt(arg)
		}
	} else if infd := int(os.Stdin.Fd()); term.IsTerminal(infd) {
		for {
			fmt.Print("Secret (empty exits):")
			arg := mustRet(term.ReadPassword(infd))
			fmt.Println()
			if len(arg) == 0 {
				break
			}
			encrypt(string(arg))
		}
	}
}

func encrypt(arg string) {
	buf := memguard.NewBufferFromBytes([]byte(arg))
	var sec yasec.Secret
	must(sec.Set(buf))
	txt := string(mustRet(sec.MarshalText()))
	switch cfg.format {
	case "json":
		json.NewEncoder(os.Stdout).Encode(txt)
	case "yaml":
		yaml.NewEncoder(os.Stdout).Encode(txt)
	default:
		if cfg.format != "" {
			log.Printf("unknown format '%s', use plain string", cfg.format)
		}
		fmt.Println(string(txt))
	}
}

func decrypt(arg string) {
	switch cfg.format {
	case "json":
		must(json.Unmarshal([]byte(arg), &arg))
	case "yaml":
		must(yaml.Unmarshal([]byte(arg), &arg))
	}
	var sec yasec.Secret
	must(sec.UnmarshalText([]byte(arg)))
	buf := mustRet(sec.Open())
	fmt.Println(buf.String())
}

func sendPassword(args []string) {
	if len(args) < 1 {
		log.Fatal("no method to send password")
	}
	switch args[0] {
	case "unix":
		sendUnixDomain(args[1:])
	default:
		log.Fatalf("unsupported password target '%s'", args[0])
	}
}

func salt() []byte {
	if cfg.Salt != "" {
		return mustRet(hex.DecodeString(cfg.Salt))
	}
	if salt := os.Getenv("YASEC_SALT"); salt != "" {
		return mustRet(hex.DecodeString(salt))
	}
	return nil
}

func must(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func mustRet[T any](v T, err error) T {
	must(err)
	return v
}
