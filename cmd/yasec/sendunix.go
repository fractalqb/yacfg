package main

import (
	"fmt"
	"log"
	"net"
	"os"

	"golang.org/x/term"
)

func sendUnixDomain(args []string) {
	fmt.Print("password:")
	pw := mustRet(term.ReadPassword(int(os.Stdin.Fd())))
	defer clear(pw)
	fmt.Println()

	for _, uds := range args {
		log.Printf("send password to '%s'", uds)
		conn := mustRet(net.Dial("unix", uds))
		mustRet(conn.Write(pw))
	}
}
