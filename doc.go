// Package yacfg is yet another config package. Its meant to be modular and
// supplemental to commandline parsing which is already part of the standard
// library. The focus of yacfg is to populate configration data structures from
// environment variables or configuration files.
//
// The examples package contains executables that show how to use it.
package yacfg
